function view(id) {
    window.location.href = "/system-serve/dbTable/edit?id=" + id;
}


function add() {
    window.location.href = "/system-serve/dbTable/edit";
}

function del(id) {
    if (confirm('你确定要删除吗？')) {
        window.location.href = "/system-serve/dbTable/del?id=" + id;
    }
}

function viewChart(id) {
    window.location.href = "/system-serve/dbTable/viewChart?id=" + id;
}

function cancel() {
    history.back();
}
