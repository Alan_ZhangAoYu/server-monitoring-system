# Linux服务器监控项目文档

## 引言

### 项目背景

> 此项目设计思想为新一代极简运维监控系统，提倡快速部署，降低运维学习难度，全自动化运行，无模板和脚本。

### 项目目标

- 可以实时监控服务器运行时状态
- 能够方便直观地显示服务器运行时的瓶颈
- 对服务器的警告信息可以很方便地通知运维人员
- 对服务器的端口以及运行的进程进行方便快捷的管理
- 对数据库(数据源)信息进行管理
- 查看被监控服务器的系统信息

## 需求分析

> 该项目基于微服务springboot架构开发，是轻量高性能的分布式监控系统，核心采集指标包括：**cpu使用率，cpu温度，内存使用率，磁盘容量，磁盘IO，硬盘SMART健康状态，系统负载，连接数量，网卡流量，硬件系统信息等。支持监测服务器上的进程应用、文件防篡改、端口、日志、DOCKER容器、数据库、数据表等资源。支持监测服务接口API、数通设备（如交换机、路由器、打印机）等。自动生成网络拓扑图，大屏可视化，web SSH（堡垒机），统计分析图表，指令下发批量执行，告警信息推送（如邮件）**。

### 项目功能规定

- 1.采用服务端和代理端协同工作方式，更轻量，更高效。
- 2.server端负责接受数据，处理数据，生成图表展示。agent端默认每隔2分钟(时间可调)上报主机指标数据。
- 3.支持主流服务器平台安装部署，如Linux, Windows等。
- 4.采用主流技术框架SpringBoot+Bootstrap，完美实现了分布式监控系统。

### 运行环境规定

- 1.JDK版本：JDK1.8、JDK11

- 2.数据库：MySql5.5及以上

- 3.支持操作系统平台

  > 支持监测Linux系列：Debian、RedHat、CentOS、ubuntu、麒麟、统信、龙芯、树莓派等
  >
  > 支持监测windows系列：Windows Server 2008 R2，2012，2016，2019，Windows 7，Windows 8，Windows 10

## 项目概要设计说明

### 数据库模型设计

![数据库ER图](./demo/sql.jpg)

## 项目部署步骤

- Linux系统
  1. 安装Java环境
  
  2. 安装MySQL数据库
  
  3. 在MySQL中执行项目目录中的`sql/servermonitoringdb.sql`文件
  
  4. 在一个终端中`cd`到项目jar包所在的目录
  
  5. 执行
     ```shell
     java -jar system-server-release.jar
     ```
     
  6. 在另一个终端中`cd`到项目jar包所在的目录
  
  7. 执行
  
     ```shell
     java -jar system-agent-release.jar
     ```
  
  8. 若出现端口占用情况请先杀掉占用端口的进程(**server默认端口为9999、agent默认端口为9998**)

- windows系统

  同上

## 项目主要参数

- 后台登录地址
  
  > http://localhost:9999/system-serve
  
- 5.默认管理员账号`admin`、 密码 `111111`

- system-server默认端口为9999、system-agent默认端口为9996
## 项目运行截图

![系统首页](./demo/demo1.jpg)

![监控主面板](./demo/demo2.jpg)

![监控主机列表](./demo/demo3.jpg)

![](./demo/demo9.jpg)

![监控主机告警报表](./demo/report.jpg)

![监控主机状态趋势图](./demo/demo4.jpg)

![网络拓扑图](./demo/tpdemo.jpg)

![主机画像图](./demo/huaxiang.jpg)

## 通信图示例（http协议）

![通信图示例](./demo/tongxin.jpg)

